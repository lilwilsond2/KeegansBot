from helper import beautifyList, getDatabaseConnection

SKILL_QUERY = "select " \
              "  armorText.name as name, " \
              "  skilltree_text.name as skill, " \
              "  armor_skill.level as level " \
              "from armor_skill " \
              "  join armor a on armor_skill.armor_id = a.id " \
              "  join armor_text armorText on a.id = armorText.id " \
              "  join skilltree_text on armor_skill.skilltree_id = skilltree_text.id " \
              "where armorText.lang_id = 'en' " \
              "  and skilltree_text.lang_id = 'en' " \
              "  and armor_id in " \
              "    (select " \
              "      armorSkill.armor_id as armorId " \
              "     from armor_skill armorSkill " \
              "       join skilltree skill on armorSkill.skilltree_id = skill.id " \
              "       join skilltree_text skillText on skill.id = skillText.id " \
              "     where skillText.name = :skillName COLLATE NOCASE)"


def getFormattedArmorOutput(skill):
    armorPieces = searchArmors(skill)
    return beautifyList(sorted(dictToList(armorPieces)))


def searchArmors(skill):
    armorPieces = getDatabaseConnection().execute(SKILL_QUERY, {"skillName": skill})
    return combineArmorsIntoDict(armorPieces)


def combineArmorsIntoDict(armorPieces):
    output = {}
    for x in armorPieces:
        if x[0] not in output:
            output[x[0]] = [(x[1], x[2])]
        else:
            output[x[0]].append((x[1], x[2]))
    return output


def dictToList(armorPieces):
    output = []
    for armor, skills in armorPieces.items():
        skillString = ""
        for skill in skills:
            skillString += "{}({}), ".format(skill[0], skill[1])
        skillString = skillString.rstrip(", ")
        output.append("{} : {}".format(armor, skillString))
    return output
